package com.zabu.kyimoecho.maze;

import android.os.AsyncTask;
import android.widget.Button;
import android.widget.Toast;
import java.util.LinkedList;
import java.util.Queue;


/**
 * Created by kyimoecho on 16-02-17.
 * Handles finding path inside maze
 */
public class TraverseTask extends AsyncTask<String, String, String> {


    private Queue<Position> notExplore;
    private Position solution;

    @Override
    protected String doInBackground(String... params) {
        Position currentPos = new Position(Integer.parseInt(params[0]),Integer.parseInt(params[1]));

        notExplore =new LinkedList<>();
        notExplore.add(currentPos);

        do{
            currentPos = notExplore.remove();
            if(currentPos.getState() == State.dest_state)
            {
                updateNext(currentPos);
                break;
            }
            expand(currentPos);
        }while(notExplore.size() != 0);

        if(currentPos.getState() != State.dest_state) return State.no_solution;

        do{
            try {
                publishProgress();
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while(solution != null);

       return State.found_solution;
    }


    //Update next pointer of nodes for playback
    private void updateNext(Position currentPos)
    {
        do{
            Position temp = currentPos.getPrev();
            temp.setNext(currentPos);
            currentPos = temp;
        }while(currentPos.getPrev() != null);

        solution = currentPos;
    }

    //Expand different path
    private void expand(Position currentPos)
    {
        Integer forward = -1;
        Integer backward = -1;
        Integer up = -1;
        Integer down = -1;
        Integer row = 0;
        Integer col = 0;

        if(currentPos.getCol()+1 < State.mazeWidth)
            forward = State.mazeArrayRepresentation.get(currentPos.getRow()).get(currentPos.getCol()+1);
        if(currentPos.getRow()+1 < State.mazeHeight)
            down = State.mazeArrayRepresentation.get(currentPos.getRow() + 1).get(currentPos.getCol());
        if(currentPos.getCol()-1 >= 0)
            backward = State.mazeArrayRepresentation.get(currentPos.getRow()).get(currentPos.getCol()-1);
        if(currentPos.getRow()-1 >= 0)
            up = State.mazeArrayRepresentation.get(currentPos.getRow() - 1).get(currentPos.getCol());


        if((forward == State.path_state) || (forward == State.dest_state)) {
            notExplore.add(new Position(currentPos.getRow(), currentPos.getCol() + 1, forward, currentPos, null));
            row = currentPos.getRow();
            col = currentPos.getCol() + 1;
            State.mazeArrayRepresentation.get(row).set(col,State.visited);
        }
        if((backward == State.path_state)  || (backward == State.dest_state)) {
            notExplore.add(new Position(currentPos.getRow(), currentPos.getCol() - 1, backward, currentPos, null));
            row = currentPos.getRow();
            col = currentPos.getCol() - 1;
            State.mazeArrayRepresentation.get(row).set(col,State.visited);
        }
        if((down == State.path_state)  || (down == State.dest_state)) {
            notExplore.add(new Position(currentPos.getRow() + 1, currentPos.getCol(), down, currentPos, null));
            row = currentPos.getRow()+1;
            col = currentPos.getCol();
            State.mazeArrayRepresentation.get(row).set(col,State.visited);
        }
        if((up == State.path_state)  || (up == State.dest_state)) {
            notExplore.add(new Position(currentPos.getRow() - 1, currentPos.getCol(), up, currentPos, null));
            row = currentPos.getRow()-1;
            col = currentPos.getCol();
            State.mazeArrayRepresentation.get(row).set(col,State.visited);
        }


    }


    @Override
    protected void onProgressUpdate(String... values) {
        updateUI();
    }

    private void updateUI() {

        int index = (State.mazeWidth*solution.getRow()) + solution.getCol();
        Button cell = (Button)State.maze.getChildAt(index);
        if((solution.getPrev() != null ) && (solution.getState() != State.dest_state)){

            cell.setBackgroundColor(State.solColor);
        }

        solution = solution.getNext();

    }

    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(State.main, result, Toast.LENGTH_LONG).show();
        State.solveResetButton.setEnabled(true);

    }
}

