package com.zabu.kyimoecho.maze;

import java.util.ArrayList;
import android.app.Activity;
import android.widget.GridLayout;
import android.widget.Button;
import android.graphics.Color;
/**
 * Created by kyimoecho on 16-02-17.
 */
public class State {
    static final String start = "S";
    static final String dest ="D";
    static final int wallColor = Color.RED;
    static final int pathColor = Color.LTGRAY;
    static final int startColor = Color.GREEN;
    static final int destColor = Color.BLACK;
    static final int solColor = Color.YELLOW;
    static final Integer path_state = 0;
    static final Integer wall_state = 1;
    static final Integer start_state = 2;
    static final Integer dest_state = 3;
    static final Integer visited = 4;
    static final String no_solution = "No Solution";
    static final String found_solution = "Found Solution";
    static final Integer mazeWidth = 10; //Change this value to reduce of expand the maze width
    static final Integer mazeHeight = 15; //Change this value to reduce of expand the mage height
    static final Integer cellSize = 35;
    static String start_dest = "";
    static ArrayList<ArrayList<Integer>> mazeArrayRepresentation;
    static GridLayout maze;
    static Button solveResetButton;
    static Activity main;


    public static void findDestination(Position startPos)
    {
        TraverseTask asyncTask = new TraverseTask();

        asyncTask.execute(startPos.getRow().toString(), startPos.getCol().toString());
    }


}


