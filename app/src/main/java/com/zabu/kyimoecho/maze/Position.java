package com.zabu.kyimoecho.maze;

/**
 * Created by kyimoecho on 16-02-17.
 */
public class Position {
    private Integer row;
    private Integer col;
    private Integer state;
    private Position prev;
    private Position next;

    public Position(int r,int c) {
        row = r;
        col = c;
        state = State.visited;
    }

    public Position(int r,int c,int s, Position argPrev, Position argNext) {
        row = r;
        col = c;
        state = s;
        prev = argPrev;
        next = argNext;
    }

    public Integer getRow(){return row;}
    public Integer getCol(){return col;}
    public Integer getState(){return state;}
    public Position getPrev(){return prev;}
    public Position getNext(){return next;}

    public void setRow(Integer r){ this.row = r; }
    public void setCol(Integer c){ this.col = c; }
    public void setState(Integer s){ this.state = s; }
    public void setPrev(Position prv){ this.prev= prv; }
    public void setNext(Position nxt){ this.next = nxt; }
}
